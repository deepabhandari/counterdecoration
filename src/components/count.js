import React, { Component } from 'react'
import '../App.css'
export default class Count extends Component {
    render(){
        let { title, task } = this.props

        return(
            <button  onClick = { task }>{ title }</button>
        )}
}
