import React, { Component } from 'react';
import Count from './components/count';
import  './App.css'
export default class App extends Component {

  constructor(){
    super();
    this.state= {
      value:0
    }
  }

  incrementCounter = () => {
    this.setState({
      value:this.state.value+1
    })
  }

  decrementCounter= () => {
    this.setState({
        value:this.state.value-1
    })
  }

  render() {
    let { value } =this.state
    return (
        <div style={{color: 'white' }}>

          <h2>Your number: { value } </h2>
          <Count
              title = { "Increase" }
              task = { () => this.incrementCounter() }

          />
          <Count

              title = { "Decrease" }
              task = { () => this.decrementCounter() }
          />
        </div>
    );
  }

}